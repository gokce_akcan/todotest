Feature: Sample Feature

  Background:
    Given I wait upto 5 seconds for application to start
    And I wait upto 5 seconds
    Then I see date on splash page
    #And I increase case count


  Scenario: As a valid user I start app and add a OTHERS reminder
    Then I touch "SPBrUILabeIOS" with text "COMPLETED"
    Then I see compeleted tasks on page

  Scenario: As a valid user I start app and add a SHOPPING reminder
    When I touch "UIImageView" with index 8
    Then I see "SHOPPING" on page
    When I enter "do shopping" in "Summary"
    And I enter "do shopping details" in "Add details"
    Then I touch "SPBrUILabeIOS" with text "Add Reminder"
    And I set date to next day
    Then I touch "UILabel" with text "OK"
    Then I assign time to 08 05
    Then I touch "UILabel" with text "OK"
    Then I touch "SPBrUILabeIOS" with text "SHOPPING"
    Then I see "ADD SHOPPING TASK" on page
    Then I touch "UIButton" with index 1
    And I wait upto 3 seconds
    Then I touch "SPBrUILabeIOS" with text "do shopping"
    And I wait upto 3 seconds
    Then I touch "UIButton" with index 2
    And I wait upto 3 seconds
    Then I touch "SPBrUILabeIOS" with text "COMPLETED"
    Then I touch "SPBrUILabeIOS" with text "do shopping"
    And I wait upto 2 seconds
    Then I touch "UIButton" with index 1
    And I wait upto 2 seconds
    Then I should not see "do shopping" on the page
    And I wait upto 2 seconds

