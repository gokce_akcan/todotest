require 'date'

Then(/^I see date on splash page$/) do
  day = ''
  if Date.today.monday?
    day = 'Monday'
  elsif Date.today.tuesday?
    day = 'Tuesday'
  elsif Date.today.wednesday?
    day = 'Wednesday'
  elsif Date.today.thursday?
    day = 'Thursday'
  elsif Date.today.friday?
    day = 'Friday'
  elsif Date.today.saturday?
    day = 'Saturday'
  elsif Date.today.sunday?
    day = 'Sunday'
  end
  date_on_page = Date.today.strftime('%B %-d, %Y')

  increase_case_count

  if query("* marked:'#{day}'").size == 0
    error_name = "No_day"
    device = get_device
    screenshot({:name => "screenshots/#{Time.now.strftime('%d%b')}/#{device[:name]}/#{error_name}"})
    p "Unable to find #{day} on splash page"
    increase_error_count(error_name)
  end

  increase_case_count
  if query("* marked:'#{date_on_page}'").size == 0
    error_name = "Unable_to_find_date"
    device = get_device
    screenshot({:name => "screenshots/#{Time.now.strftime('%d%b')}/#{device[:name]}/#{error_name}"})
    p "Unable to find #{date_on_page} on splash page"
    increase_error_count(error_name)
  end
end

Then(/^I see "([^"]*)" on page$/) do |text|
  increase_case_count
  if query("* marked:'#{text}'").empty?
    error_name = "No_#{text}"
    device = get_device
    screenshot({:name => "screenshots/#{Time.now.strftime('%d%b')}/#{device[:name]}/#{error_name}"})
    p "Unable to find #{text} on page"
    increase_error_count(error_name)
  end
end

Then(/^I enter "([^"]*)" in "([^"]*)"$/) do |str, place|
  touch("UITextFieldLabel marked:'#{place}'")
  wait_for_keyboard
  keyboard_enter_text(str)
  hide_soft_keyboard
end

And(/^I set date to next day$/) do
  year = Date.today.year
  month = Date.today.month
  day = Date.today.day + 1
  touch("UIDatePickerContentView marked:'#{day}'")
  touch("UIDatePickerContentView marked:'#{month}'")
  touch("UIDatePickerContentView marked:'#{year}'")
  touch("UIButton marked:'OK'")
end

Then(/^I assign time to 08 05 $/) do
  touch("UILabel marked:'08'")
  touch("UILabel marked:'05'")
end

Then(/^I see compeleted tasks on page$/) do
  increase_case_count
  if query('SPBrUILabeIOS').size<5
    error_name = "No_completed_task"
    device = get_device
    screenshot({:name => "screenshots/#{Time.now.strftime('%d%b')}/#{device[:name]}/#{error_name}"})
    increase_error_count(error_name)
    p 'There is no completed task on page '
  end
end

Then(/^I should not see "([^"]*)" on the page$/) do |str|
  increase_case_count
  if query('SPBrUILabeIOS').size<5
    error_name = "#{str}_should_not_be_exist"
    device = get_device
    screenshot({:name => "screenshots/#{Time.now.strftime('%d%b')}/#{device[:name]}/#{error_name}"})
    increase_error_count(error_name)
    p 'There is no completed task on page '
  end
end

Then(/^I wait upto (\d+) seconds$/) do |sec|
  sleep sec.to_i
end