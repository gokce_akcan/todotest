Feature: To Do app is being tested

  Background:
    Given I wait upto 5 seconds for the app to start
    And I wait for 5 seconds
    Then I see date on splash page
    #And I increase case count

  Scenario: As a valid user I start app and add a OTHERS reminder
    Then I touch "android.widget.TextView" with text "COMPLETED"
    Then I see compeleted tasks on page
    And I wait for 5 seconds

  Scenario: As a valid user I start app and add a SHOPPING reminder
    When I touch "ImageView" with index 9
    Then I see "SHOPPING" on page
    When I enter "do shopping" in "Summary"
    And I enter "do shopping details" in "Add details"
    Then I touch "AutoFontSizeTextView" with text "Add Reminder"
    Then I assign date to next day
    Then I touch "android.widget.Button" with text "OK"
    Then I assign time to 11 30
    And I touch "OK" or "Done" or "Tamam" button
    Then I touch "android.widget.TextView" with text "SHOPPING"
    Then I see "ADD SHOPPING TASK" on page
    Then I touch "android.widget.TextView" with index 13
    And I wait for 3 seconds
    Then I touch "android.widget.TextView" with text "do shopping"
    And I wait for 3 seconds
    Then I touch "android.widget.TextView" with index 8
    And I wait for 3 seconds
    Then I touch "android.widget.TextView" with text "COMPLETED"
    Then I touch "android.widget.TextView" with text "COMPLETED"
    Then I touch "android.widget.TextView" with text "do shopping"
    And I wait for 2 seconds
    Then I touch "android.widget.TextView" with index 7
    And I wait for 2 seconds
    Then I should not see "do shopping" on the page
    And I wait for 2 seconds

  Scenario: As a valid user I start app and add a PAYMENT reminder
    When I touch "ImageView" with index 9
    Then I see "SHOPPING" on page
    When I enter "PAYMENT" in "Summary"
    And I enter "PAYMENT details" in "Add details"
    Then I touch "AutoFontSizeTextView" with text "Add Reminder"
    Then I assign date to next day
    Then I touch "android.widget.Button" with text "OK"
    Then I assign time to 11 30
    And I touch "OK" or "Done" or "Tamam" button
    Then I touch "android.widget.TextView" with text "PAYMENT"
    Then I see "ADD PAYMENT TASK" on page
    Then I touch "android.widget.TextView" with index 13
    And I wait for 3 seconds
    Then I touch "android.widget.TextView" with text "PAYMENT"
    And I wait for 3 seconds
    Then I touch "android.widget.TextView" with index 8
    And I wait for 3 seconds
    Then I touch "android.widget.TextView" with text "COMPLETED"
    Then I touch "android.widget.TextView" with text "PAYMENT"
    And I wait for 2 seconds
    Then I touch "android.widget.TextView" with index 7
    And I wait for 2 seconds
    Then I should not see "PAYMENT" on the page
    And I wait for 2 seconds

  Scenario: As a valid user I start app and add a FAMILY reminder
    When I touch "ImageView" with index 9
    Then I see "SHOPPING" on page
    When I enter "FAMILY" in "Summary"
    And I enter "FAMILY details" in "Add details"
    Then I touch "AutoFontSizeTextView" with text "Add Reminder"
    Then I assign date to next day
    Then I touch "android.widget.Button" with text "OK"
    Then I assign time to 11 30
    And I touch "OK" or "Done" or "Tamam" button
    Then I touch "android.widget.TextView" with text "FAMILY"
    Then I see "ADD FAMILY TASK" on page
    Then I touch "android.widget.TextView" with index 13
    And I wait for 3 seconds
    Then I touch "android.widget.TextView" with text "FAMILY"
    And I wait for 3 seconds
    Then I touch "android.widget.TextView" with index 8
    And I wait for 3 seconds
    Then I touch "android.widget.TextView" with text "COMPLETED"
    Then I touch "android.widget.TextView" with text "FAMILY"
    And I wait for 2 seconds
    Then I touch "android.widget.TextView" with index 7
    And I wait for 2 seconds
    Then I should not see "FAMILY" on the page
    And I wait for 2 seconds

  Scenario: As a valid user I start app and add a FRIENDS reminder
    When I touch "ImageView" with index 9
    Then I see "SHOPPING" on page
    When I enter "FRIENDS" in "Summary"
    And I enter "FRIENDS details" in "Add details"
    Then I touch "AutoFontSizeTextView" with text "Add Reminder"
    Then I assign date to next day
    Then I touch "android.widget.Button" with text "OK"
    Then I assign time to 11 30
    And I touch "OK" or "Done" or "Tamam" button
    Then I touch "android.widget.TextView" with text "FRIENDS"
    Then I see "ADD FRIENDS TASK" on page
    Then I touch "android.widget.TextView" with index 13
    And I wait for 3 seconds
    Then I touch "android.widget.TextView" with text "FRIENDS"
    And I wait for 3 seconds
    Then I touch "android.widget.TextView" with index 8
    And I wait for 3 seconds
    Then I touch "android.widget.TextView" with text "COMPLETED"
    Then I touch "android.widget.TextView" with text "FRIENDS"
    And I wait for 2 seconds
    Then I touch "android.widget.TextView" with index 7
    And I wait for 2 seconds
    Then I should not see "FRIENDS" on the page
    And I wait for 2 seconds

  Scenario: As a valid user I start app and add a WORK reminder
    When I touch "ImageView" with index 9
    Then I see "SHOPPING" on page
    When I enter "WORK" in "Summary"
    And I enter "WORK details" in "Add details"
    Then I touch "AutoFontSizeTextView" with text "Add Reminder"
    Then I assign date to next day
    Then I touch "android.widget.Button" with text "OK"
    Then I assign time to 11 30
    And I touch "OK" or "Done" or "Tamam" button
    Then I touch "android.widget.TextView" with text "WORK"
    Then I see "ADD WORK TASK" on page
    Then I touch "android.widget.TextView" with index 13
    And I wait for 3 seconds
    Then I touch "android.widget.TextView" with text "WORK"
    And I wait for 3 seconds
    Then I touch "android.widget.TextView" with index 8
    And I wait for 3 seconds
    Then I touch "android.widget.TextView" with text "COMPLETED"
    Then I touch "android.widget.TextView" with text "WORK"
    And I wait for 2 seconds
    Then I touch "android.widget.TextView" with index 7
    And I wait for 2 seconds
    Then I should not see "WORK" on the page
    And I wait for 2 seconds

  Scenario: As a valid user I start app and add a SPORT reminder
    When I touch "ImageView" with index 9
    Then I see "SHOPPING" on page
    When I enter "SPORT" in "Summary"
    And I enter "SPORT details" in "Add details"
    Then I touch "AutoFontSizeTextView" with text "Add Reminder"
    Then I assign date to next day
    Then I touch "android.widget.Button" with text "OK"
    Then I assign time to 11 30
    And I touch "OK" or "Done" or "Tamam" button
    Then I touch "android.widget.TextView" with text "SPORT"
    Then I see "ADD SPORT TASK" on page
    Then I touch "android.widget.TextView" with index 13
    And I wait for 3 seconds
    Then I touch "android.widget.TextView" with text "SPORT"
    And I wait for 3 seconds
    Then I touch "android.widget.TextView" with index 8
    And I wait for 3 seconds
    Then I touch "android.widget.TextView" with text "COMPLETED"
    Then I touch "android.widget.TextView" with text "SPORT
    And I wait for 2 seconds
    Then I touch "android.widget.TextView" with index 7
    And I wait for 2 seconds
    Then I should not see "SPORT" on the page
    And I wait for 2 seconds

  Scenario: As a valid user I start app and add a HOBBY reminder
    When I touch "ImageView" with index 9
    Then I see "SHOPPING" on page
    When I enter "HOBBY" in "Summary"
    And I enter "HOBBY details" in "Add details"
    Then I touch "AutoFontSizeTextView" with text "Add Reminder"
    Then I assign date to next day
    Then I touch "android.widget.Button" with text "OK"
    Then I assign time to 11 30
    And I touch "OK" or "Done" or "Tamam" button
    Then I touch "android.widget.TextView" with text "HOBBY"
    Then I see "ADD HOBBY TASK" on page
    Then I touch "android.widget.TextView" with index 13
    And I wait for 3 seconds
    Then I touch "android.widget.TextView" with text "HOBBY"
    And I wait for 3 seconds
    Then I touch "android.widget.TextView" with index 8
    And I wait for 3 seconds
    Then I touch "android.widget.TextView" with text "COMPLETED"
    Then I touch "android.widget.TextView" with text "HOBBY"
    And I wait for 2 seconds
    Then I touch "android.widget.TextView" with index 7
    And I wait for 2 seconds
    Then I should not see "HOBBY" on the page
    And I wait for 2 seconds

  Scenario: As a valid user I start app and add a OTHERS reminder
    When I touch "ImageView" with index 9
    Then I see "SHOPPING" on page
    When I enter "OTHERS" in "Summary"
    And I enter "OTHERS details" in "Add details"
    Then I touch "AutoFontSizeTextView" with text "Add Reminder"
    Then I assign date to next day
    Then I touch "android.widget.Button" with text "OK"
    Then I assign time to 11 30
    And I touch "OK" or "Done" or "Tamam" button
    Then I touch "android.widget.TextView" with text "OTHERS"
    Then I see "ADD OTHERS TASK" on page
    Then I touch "android.widget.TextView" with index 13
    And I wait for 3 seconds
    Then I touch "android.widget.TextView" with text "OTHERS"
    And I wait for 3 seconds
    Then I touch "android.widget.TextView" with index 8
    And I wait for 3 seconds
    Then I touch "android.widget.TextView" with text "COMPLETED"
    Then I touch "android.widget.TextView" with text "OTHERS"
    And I wait for 2 seconds
    Then I touch "android.widget.TextView" with index 7
    And I wait for 2 seconds
    Then I should not see "OTHERS" on the page
    And I wait for 2 seconds
