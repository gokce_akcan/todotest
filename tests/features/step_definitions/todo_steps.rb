require 'date'
require 'calabash-smartface'
require 'calabash-smartface/calabash_android_steps'

Then(/^I see date on splash page$/) do
  day = ''
  if Date.today.monday?
    day = 'Monday'
  elsif Date.today.tuesday?
    day = 'Tuesday'
  elsif Date.today.wednesday?
    day = 'Wednesday'
  elsif Date.today.thursday?
    day = 'Thursday'
  elsif Date.today.friday?
    day = 'Friday'
  elsif Date.today.saturday?
    day = 'Saturday'
  elsif Date.today.sunday?
    day = 'Sunday'
  end
  date_on_page = Date.today.strftime('%B %-d, %Y')

  increase_case_count
  if query("* marked:'#{day}'").size == 0
    error_name = "No_day"
    device = get_device(ENV["ADB_DEVICE_ARG"])
    screenshot({:name => "#{device[:model]}/#{error_name}"})
    p "Unable to find #{day} on splash page"
    increase_error_count(error_name)
  end

  increase_case_count
  if query("* marked:'#{date_on_page}'").size == 0
    error_name = "Unable_to_find_date"
    device = get_device(ENV["ADB_DEVICE_ARG"])
    screenshot({:name => "#{device[:model]}/#{error_name}"})
    p "Unable to find #{date_on_page} on splash page"
    increase_error_count(error_name)
  end
end

Then(/^I see "([^"]*)" on page$/) do |text|
  increase_case_count
  if query("* marked:'#{text}'").empty?
    error_name = "No_#{text}"
    device = get_device(ENV["ADB_DEVICE_ARG"])
    screenshot({:name => "#{device[:model]}/#{error_name}"})
    p "Unable to find #{text} on page"
    increase_error_count(error_name)
  end
end

When(/^I enter "([^"]*)" in "([^"]*)"$/) do |str, place|
  if place=="Summary"
    index=0
  else index=1
  end
  enter_text("SpEditText index:#{index}", str)
  hide_soft_keyboard
end

Then(/^I assign date to next day$/) do
  year = Date.today.year
  month = Date.today.month
  day = Date.today.day + 1
  query("DatePicker index:0", :method_name => 'updateDate', :arguments => [year, month-1, day])
end

Then(/^I assign time to (\d+) (\d+)$/) do |hour, minute|
  if query('TimePicker').size > 0
    set_time('TimePicker', hour.to_i, minute.to_i)
  end
end

Then(/^I see compeleted tasks on page$/) do
  increase_case_count
  if query('android.widget.TextView').size<5
    error_name = "No_completed_task"
    device = get_device(ENV["ADB_DEVICE_ARG"])
    screenshot({:name => "#{device[:model]}/#{error_name}"})
    increase_error_count(error_name)
    p 'There is no completed task on page '
  end
end



Then(/^I should not see "([^"]*)" on the page$/) do |str|
  increase_case_count
  if query('android.widget.TextView').size<5
    error_name = "#{str}_should_not_be_exist"
    device = get_device(ENV["ADB_DEVICE_ARG"])
    screenshot({:name => "#{device[:model]}/#{error_name}"})
    increase_error_count(error_name)
    p 'There is no completed task on page '
  end
end


