require 'calabash-android/management/adb'
require 'calabash-android/operations'
require 'calabash-smartface/calabash_android_steps'

Before do |scenario|
  wake_up
  start_test_server_in_background
  do_before_scenario
  $do_crop = false
end

After do |scenario|
  test_name = 'sample-app-todo-test'
  device = get_device(ENV["ADB_DEVICE_ARG"])
  shutdown_test_server
  results_path = 'C:/JenkinsWorks/workspace/Test Results/Calabash-Android Device Tests'
  test_root = 'C:/JenkinsWorks/workspace/Calabash-Android Device Tests'
  screenshot_path = "#{test_root}/#{test_name}/tests/screenshots/#{Time.now.strftime('%d%b')}/#{device[:serial]}_#{device[:model]}"
  device_name = save_results_to_file(results_path, test_name)
  save_report_on_azure("#{test_root}/#{test_name}/tests/control_images/#{device[:model]}", screenshot_path, "#{test_name} on #{device[:model]}", results_path, device_name)
  do_after_scenario
end